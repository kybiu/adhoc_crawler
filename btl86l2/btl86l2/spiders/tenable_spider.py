import scrapy
from bs4 import BeautifulSoup as BS


def cleanhtml(raw_html):
    cleantext = BS(raw_html, "lxml").text
    return cleantext


class TenableSpider(scrapy.Spider):
    name = 'tenable'

    def start_requests(self):
        urls = [
            'https://www.tenable.com/plugins/newest?type=&page=1'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_tenable, cb_kwargs={"url_index": 1})

    def parse_tenable(self, response, url_index):
        topics = response.xpath("//tr")

        for topic in topics:
            try:
                topic_td = topic.xpath(".//td")
            except:
                continue
            if len(topic_td) == 0:
                continue

            id = cleanhtml(topic_td[0].get())
            name = cleanhtml(topic_td[1].get())
            product = cleanhtml(topic_td[2].get())
            family = cleanhtml(topic_td[3].get())
            publish = cleanhtml(topic_td[4].get())
            severity = cleanhtml(topic_td[5].get())
            yield {
                'id': id,
                'name': name,
                'product': product,
                'family': family,
                'publish': publish,
                'severity': severity,
            }

        url_index += 1
        next = "https://www.tenable.com/plugins/newest?type=&page=" + str(url_index)
        yield scrapy.Request(url=next, callback=self.parse_tenable, cb_kwargs={"url_index": url_index})
