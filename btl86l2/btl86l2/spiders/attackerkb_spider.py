import scrapy


class AttackerKbSpider(scrapy.Spider):
    name = "attackerkb"

    def start_requests(self):
        urls = [
            'https://attackerkb.com/?page=0',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_attacker, cb_kwargs={"url_index": 0})

    def parse_attacker(self, response, url_index):
        topics = response.css('div.topic-feature-container')
        if len(topics) == 0:
            return
        for index, topic in enumerate(topics):
            attacker_value = topic.css('.attacker-value-label h5::text').get()
            topic_title = topic.css('.col-12.title h3 a::text').get()
            reference_and_date = topic.css('.reference-and-date::text').get()
            reference_and_date = reference_and_date.replace("\n", "").strip()
            description = topic.css('.col-12.description::text').get()
            description = description.replace("\n", "").strip()

            yield {
                "attacker_value": attacker_value,
                "topic_title": topic_title,
                "reference_and_date": reference_and_date,
                "description": description
            }

        url_index += 1
        next = "https://attackerkb.com/?page=" + str(url_index)
        yield scrapy.Request(url=next, callback=self.parse_attacker, cb_kwargs={"url_index": url_index})
